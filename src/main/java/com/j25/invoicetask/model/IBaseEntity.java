package com.j25.invoicetask.model;

public interface IBaseEntity {
    Long getId();
}
