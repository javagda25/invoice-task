package com.j25.invoicetask.model;

public enum TaxType {
    PRODUCT,
    SERVICE
}
